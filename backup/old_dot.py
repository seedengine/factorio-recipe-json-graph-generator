#graph {
# n1 [Label = "n1"];
# n2 [Label = "n2"];
# n3 [Label = "n3"];
# n1 -- n2;
# n1 -- n3;
# n2 -- n2;
#}


import json
from datetime import datetime
import ResourceList

def writeSubGraph(file, name, productList, ingredientList):
	file.write(" subgraph " + name.replace("-", "") + " {\n")
	for product in productList:
		writeNode(file, product)
	for edge in ingredientList:
		writeEdge(file, edge, name)
	file.write(" }\n")
	
def writeRank(file, rank, nodeList):
	file.write("{ rank = same; " + str(rank) + "; ")
	for node in nodeList:
		writeNode(file, node)
	file.write("}\n")
def writeNode(file, nodeID, color = "black"):
	file.write(" " + nodeID.replace("-", "") + " [Label = \"" + nodeID.replace("-", " ") + "\", color = \"" + color + "\"];\n");
def writeEdge(file, left, right):
	file.write(" " + left.replace("-", "") + " -> " + right.replace("-", "") + ";\n");

def main():
	substanceTierList = []
	substanceSet = []
	lastSubstanceSet = []
	
	sciencePackList = [
		"advanced-logistic-science-pack", 
		"logistic-science-pack", 
		"chemical-science-pack", 
		"automation-science-pack",
		"military-science-pack",
		"production-science-pack",
		"utility-science-pack",
		"alien-science-pack"
	]
	#, "alien-science-pack-blue", "alien-science-pack-green", "alien-science-pack-orange", "alien-science-pack-purple", "alien-science-pack-red", "alien-science-pack-yellow"
	
	substanceSet = [
		"copper-ore",
		"iron-ore",
		"water",
		"coal",
		"wood",
		"stone",
		"crude-oil",
		"uranium-ore",
		"steam",
		"used-up-uranium-fuel-cell"
	]
	
	substanceTierList.append(substanceSet);
	
	last = datetime.now().timestamp() * 1000
	
	with open( "vanilla-recipes.json", "r") as f:
		datasheet = json.load(f)
	#if not k.startswith("y") and not ("barrel" in k) and not ("void" in k)
	remainingSet = {k:v for (k, v) in datasheet.items() if 
		v["category"] != "fluid-filter" and 
		v["category"] != "barreling-pump" and 
		not v["name"].startswith("ye_") and 
		not v["name"].startswith("yie_") and 
		not v["name"].startswith("ye-") and 
		not v["name"].startswith("y-") and 
		not v["name"].startswith("y_") and
		not v["name"].startswith("yi_") and
		not v["name"].startswith("yi-") and
		not "from-y-" in v["name"] and
		not "from-ye_" in v["name"] and
		not "from-yie_" in v["name"] and
		not "from-ye-" in v["name"] and
		not "from-y_" in v["name"] and
		not "from-yi_" in v["name"] and
		v["category"] != "angels-water-void"}
	
	now = datetime.now().timestamp() * 1000
	print("Eliminated Yuki and Void recipes in " + str(now - last) + " milliseconds")
	last = now
	
	print("Building Graph with " + str(len(substanceTierList[0])) + " Root Items.")
	print("Going to evaluate " + str(len(remainingSet)) + " recipes.")
	
	graph = ResourceList.ResourceList()
	graph.addResource("copper-ore", 10)
	graph.addResource("iron-ore", 10)
	graph.addResource("water", 1)
	graph.addResource("coal", 10)
	graph.addResource("wood", 50)
	graph.addResource("stone", 10)
	graph.addResource("crude-oil", 20)
	graph.addResource("uranium-ore", 20)
	graph.addResource("steam", 10)
	graph.addResource("used-up-uranium-fuel-cell", 1)
	
	while True:
		newRecipes = {k:v for (k, v) in remainingSet.items() if set([c["name"] for c in v["ingredients"]]) <= set(substanceSet)}
		if (len(newRecipes.keys()) == 0):
			break
		for key in newRecipes.keys():
			remainingSet.pop(key, None)
			
		for (entry, value) in newRecipes.items():
			graph.createRecipeFromDictionary(value)
			
		newProducts = []
		for (k, v) in newRecipes.items():
			newProducts = newProducts + v["products"]
		newSubstances = [v["name"] for v in newProducts]
		substanceTierList.append([v for (k, v) in newRecipes.items()])
		substanceSet = list(set(substanceSet + newSubstances))
	
	i = 0
	for tier in substanceTierList:
		i += len(tier)
	print("Sorted " + str(i) + " Recipes into " + str(len(substanceTierList)) + " Tiers.")
	print(str(len(remainingSet)) + " Recipes got left over or couldn't be associated with the root Items.")
	now = datetime.now().timestamp() * 1000
	print("This took " + str(now - last) + " milliseconds")
	last = now
	
	if (len(remainingSet) > 0):
		with open("remaining.json", "w+") as f:
			json.dump(remainingSet, f, indent=4)
	
	print("Dumped remaining recipes to remaining.json")
	
	
	print("Minimum cost values of science packs:")
	for pack in sciencePackList:
		obj = graph.getResourceFromName(pack)
		if (not isinstance(obj, type(None))):
			print(pack + ": " + str(int(obj.getResourceValue())))
	
	"""
	
	while not len(lastSubstanceSet) == len(substanceSet):
		newRecipes = {k:v for (k, v) in remainingSet.items() if isContained([c["name"] for c in v["ingredients"]], substanceSet)}
		for key in newRecipes.keys():
			remainingSet.pop(key, None)
		newProducts = []
		for (k, v) in newRecipes.items():
			newProducts = newProducts + v["products"]
		newSubstances = [v["name"] for v in newProducts]
		substanceTierList.append([v for (k, v) in newRecipes.items()])
		lastSubstanceSet = substanceSet
		substanceSet = list(set(substanceSet + newSubstances))
	
	
	i = 0
	for tier in substanceTierList:
		i += len(tier)
	print("Sorted " + str(i) + " Recipes into " + str(len(substanceTierList)) + " Tiers.")
	print(str(len(remainingSet)) + " Recipes got left over or couldn't be associated with the root Items.")
	
	if (len(remainingSet) > 0):
		with open("remaining.json", "w+") as f:
			json.dump(remainingSet, f, indent=4)
	
	print("Dumped remaining recipes to remaining.json")
	
	recipeNodes = []
	objectNodes = []
	
	print("Discovering most significant nodes.")
	
	for i in reversed(range(1, len(substanceTierList))):
		print("Evaluated Tier " + str(i))
		for j in range(len(substanceTierList[i])):
			recipeNodes.append(
				{
					"name":substanceTierList[i][j]["name"], 
					"ingredients":[v["name"] for v in substanceTierList[i][j]["ingredients"]], 
					"products":[v["name"] for v in substanceTierList[i][j]["products"]], 
					"tier":i
				}
			)
			
			for ingredient in substanceTierList[i][j]["products"]:
				if ingredient["name"] in [n["name"] for n in objectNodes]:
					objectNodes[[x for x, v in enumerate(objectNodes) if v["name"] == ingredient["name"]][0]]["value"] += 1
				else:
					objectNodes.append({"name":ingredient["name"], "value":1})
			
			recipeValue = sum([n["value"] for n in objectNodes if n["name"] in [v["name"] for v in substanceTierList[i][j]["products"]]])
			recipeValue /= max(len(substanceTierList[i][j]["products"]), 8)
			recipeValue *= i / 4
			recipeValue = round(recipeValue)
			
			for ingredient in substanceTierList[i][j]["ingredients"]:
				if ingredient["name"] in [n["name"] for n in objectNodes]:
					objectNodes[[x for x, v in enumerate(objectNodes) if v["name"] == ingredient["name"]][0]]["value"] += len(substanceTierList[i][j]["products"]) + len(substanceTierList[i][j]["ingredients"])
				else:
					objectNodes.append({"name":ingredient["name"], "value":recipeValue})
	
	print("Sorting evaluated nodes.")
	objectNodes.sort(key= lambda n: n["value"], reverse = True)
	print("Found most significant nodes.")
	
	for node in objectNodes:
		if (node["name"] in sciencePackList):
			print(node)
	
	with open("vanilla-graph.dot", "w+") as f:
		f.write("digraph {\n {")
		f.write(" 0")
		for i in range(1, len(substanceTierList)):
			f.write(" -> " + str(i) + " ")
		f.write(";\n")
		for node in substanceTierList[0]:
			writeNode(f, node)
		f.write("}")
		
		writeRank(f, 0, substanceTierList[0])
		for i in range(1, len(substanceTierList)):
			writeRank(f, i, [v["name"] for v in substanceTierList[i]])
			for j in range(len(substanceTierList[i])):
				for edge in substanceTierList[i][j]["ingredients"]:
					writeEdge(f, edge["name"], substanceTierList[i][j]["name"])
		#for i in range(1, len(substanceTierList) - 1):
			#for j in range(len(substanceTierList[i])):
				#writeSubGraph(f, substanceTierList[i][j]["name"], [v["name"] for v in substanceTierList[i][j]["products"]], [v["name"] for v in substanceTierList[i][j]["ingredients"]])
		
		f.write("}\n")
		
		now = datetime.now().timestamp() * 1000
		print("Wrote DOT file in " + str(now - last) + " milliseconds")
		last = now
	
	"""


if __name__ == "__main__":
	main()