from lib.graph import Graph
#basically just a graph with typised nodes

def recipeCostFunction(inputValues = [], energyCost = 1, time = 1):
	val = 1;
	for i in inputValues:
		val += i
	if (time > 0):
		val *= time
	#val *= energyCost
	return val

class ResourceObj(Graph.GraphObj):
	def __init__(self, owner, id, name, value):
		Graph.GraphObj.__init__(self, owner, id, 0, [], [])
		self.name = name
		self.value = value
	
	def isTerminal():
		return len(self.getChildren()) == 0
	
	def getName(self):
		return self.name;
		
	def getResourceValue(self, list):
		#self.updateValue(list)
		
		return self.value
		
	def addUsage(self, list, usageID):
		self.owner.addConnection(self.getID(), usageID)
		#list.computeValue(usageID)
	
	def addProduction(self, list, recipeID):
		self.owner.addConnection(recipeID, self.getID())
		#self.updateValue(list)
	
	def updateValue(self, list):
		#print(self.name)
		for recipe in self.getParents():
			if not list.contains(recipe):
				continue
			val = list.getRecipe(recipe).updateRecipeProcessCost(list)
			
			if (self.value == -1 or val > self.value):
				self.value = val
				#for updates in self.getChildren():
					#list.computeValue(updates) #propagate
		return self.value
	
		

class RecipeObj(Graph.GraphObj):
	def __init__(self, owner, id, name, inputColl = [], outputColl = [], cost = 1):
		Graph.GraphObj.__init__(self, owner, id, {}, [i[1] for i in inputColl], [i[1] for i in outputColl])
		self.input = inputColl
		self.output = outputColl
		self.value = cost
		self.name = name
		
	def getInputSet(self):
		return set([id for (value, id) in self.input])
	def getOutputCount(self):
		val = 0;
		for (outputCnt, id) in self.output:
			val += outputCnt
		return val
	
	def addInput(self, value, peer):
		self.input.append((value, peer))
		self.owner.addConnection(peer, self.getID())
		
	def addOutput(self, value, peer):
		self.output.append((value, peer))
		self.owner.addConnection(self.getID(), peer)
	
	def __add__(self, other):
		resources = {}
		for (value, id) in self.input:
			if (not id in resources.keys()): resources[id] = 0
			resources[id] -= value
		for (value, id) in other.input:
			if (not id in resources.keys()): resources[id] = 0
			resources[id] -= value
		for (value, id) in self.output:
			if (not id in resources.keys()): resources[id] = 0
			resources[id] += value
		for (value, id) in other.output:
			if (not id in resources.keys()): resources[id] = 0
			resources[id] += value
		newInput = [(value, id) for (id, value) in resources.items() if value > 0]
		newOutput = [(-value, id) for (id, value) in resources.items() if value < 0]
		newValue = self.cost + other.cost
		newName = self.name + "-merged-" + other.name
		newID = self.owner.IDCtr
		self.owner.IDCtr = self.owner.IDCtr + 1
		return RecipeObj(self.owner, newID, newName, newInput, newOutput, newValue)
	
	def getName(self):
		return self.name + "-recipe"
	def setName(self, str):
		self.name = str
	def getOutputSet(self):
		return set([id for (value, id) in self.output])
	def getRecipeCost(self):
		return self.value
	def getRecipeProcessCost(self, list):
		return recipeCostFunction([list.getResource(id).getResourceValue(list) * value for (value, id) in self.input if list.contains(id)], self.value);
	def updateRecipeProcessCost(self, list):
		return recipeCostFunction([list.getResource(id).updateValue(list) * value for (value, id) in self.input if list.contains(id)], self.value);

def verifyRecipeDictionary(data):
	if (not isinstance(data, dict)):
		return False;
	
	if (not "energy" in data.keys()):
		return False
	
	if (not "ingredients" in data.keys()):
		return False
	if (not isinstance(data["ingredients"], list)):
		return False
	for entity in data["ingredients"]:
		if (not (isinstance(entity["amount"], int) or isinstance(entity["amount"], float))):
			return False
		if (not isinstance(entity["name"], str)):
			return False
	if (not "products" in data.keys()):
		return False
	if (not isinstance(data["products"], list)):
		return False
	for entity in data["products"]:
		if (not "amount" in entity.keys() or not (isinstance(entity["amount"], int) or isinstance(entity["amount"], float))):
			return False
		if (not "name" in entity.keys() or not isinstance(entity["name"], str)):
			return False
	return True


class ResourceList(Graph.Graph):
	def __init__(self):
		Graph.Graph.__init__(self)
	
	def addRecipe(self, recipeObj):
		if (not isinstance(recipeObj, RecipeObj)):
			raise ValueError("Excepted parameter to be of type RecipeObject. Got type " + str(type(recipeObj)) + " instead.")
		
		self.nodes.append(recipeObj)
		return recipeObj.getID()
		
	def addResource(self, name, value = -1):
		if (not isinstance(name, str)):
			raise ValueError("Expected a string as name")
		
		val = self.IDCtr
		self.IDCtr = self.IDCtr + 1
		resource = ResourceObj(self, val, name, value)
		self.nodes.append(resource)
		
		return resource.getID()
	
	def deleteRecipe(self, id):
		node = self.findNode(id)
		for parent in node.getParents():
			self.deleteConnection(parent, id)
		for child in node.getChildren():
			self.deleteConnection(id, child)
			
		self.deleteNode(id)
	
	def isResource(self, id):
		obj = self.findNode(id)
		return isinstance(obj, ResourceObj)
	
	def isRecipe(self, id):
		obj = self.findNode(id)
		return isinstance(obj, RecipeObj)
		
	def getResource(self, id):
		obj = self.findNode(id)
		if (not isinstance(obj, ResourceObj)):
			raise KeyError("requested object not a resource.")
		return obj
	def getRecipe(self, id):
		obj = self.findNode(id)
		if (not isinstance(obj, RecipeObj)):
			raise KeyError("requested object not a recipe.")
		return obj
	
	def computeValue(self, id):
		obj = self.findNode(id)
		if (isinstance(obj, RecipeObj)):
			for outputID in obj.getOutputSet():
				self.computeValue(outputID)
		elif (isinstance(obj, ResourceObj)):
			obj.updateValue(self)
	
	def getResourceFromName(self, name):
		for obj in self.nodes:
			if (isinstance(obj, ResourceObj)):
				if (obj.getName() == name):
					return obj
		return None
	
	
	def createRecipeFromDictionary(self, data):
		
		if data["ingredients"] == {}:
			data["ingredients"] = []
		
		for product in data["products"]:
			if {"amount_min", "amount_max", "probability"}.issubset(product.keys()):
				product["amount"] = ((product["amount_min"] + product["amount_max"])) * product["probability"] / 2
				del product["amount_min"]
				del product["amount_max"]
				del product["probability"]
		
		
		if (not verifyRecipeDictionary(data)):
			raise ValueError("The object passed is not a valid dictionary: " + str(data))
		
		_input = []
		_output = []
		
		for ingredient in data["ingredients"]:
			amount = ingredient["amount"]
			if ("catalyst_amount" in ingredient.keys()):
				if (amount - ingredient["catalyst_amount"] > 0):
					amount = amount - ingredient["catalyst_amount"]
				else:
					amount = 0
			id = self.getResourceFromName(ingredient["name"])
			if (isinstance(id, type(None))):
				raise KeyError("Didn't find id of resource " + ingredient["name"])
			_input.append((amount, id.getID()))
		
		newObjects = []
		
		for product in data["products"]:
			amount = product["amount"]
			if ("probability" in product.keys()):
				amount *= product["probability"]
			if ("catalyst_amount" in product.keys()):
				if (amount - product["catalyst_amount"] > 0):
					amount = amount - product["catalyst_amount"]
				else:
					amount = 0
		
			id = self.getResourceFromName(product["name"])
			if (isinstance(id, type(None))):
				id = self.addResource(product["name"])
				_output.append((amount, id))
			else:
				_output.append((amount, id.getID()))
		
		val = self.IDCtr
		self.IDCtr = self.IDCtr + 1
		obj = RecipeObj(self, val, data["name"], _input, _output, data["energy"] * 10)
		
		self.addRecipe(obj)
		
		for (amount, id) in _input:
			self.getResource(id).addUsage( self, obj.getID())
		
		for (amount, id) in _output:
			self.getResource(id).addProduction( self, obj.getID())
	
	def createRecipeFromData(self, name = "", inputColl = [], outputColl = [], cost = 1):
		val = self.IDCtr
		self.IDCtr = self.IDCtr + 1
		obj = RecipeObj(self, val, name, inputColl, outputColl, cost)
		
		return self.addRecipe(obj)
	
	def getSubList(self, resource):
		lastLen = -1
		
		resLst = ResourceList()
		
		coll = [resource]
		convMap = {}
		
		recipeList = []
		ressourceList = []
		
		while(len(resLst.getNodes()) > lastLen):
			lastLen = len(resLst.getNodes())
			
			coll_tmp = []
			
			for obj in coll:
				if self.isResource(obj):
					res = self.getResource(obj)
					if not obj in convMap.keys():
						id = resLst.addResource(res.getName(), res.getValue())
						convMap[res.getID()] = id
						coll_tmp += res.getParents()
						ressourceList.append(obj)
				else:
					recipe = self.getRecipe(obj)
					recipeList.append(obj)
					for rid in recipe.getOutputSet():
						if not (rid in convMap.keys()):
							res = self.getResource(rid)
							id = resLst.addResource(res.getName(), res.getValue())
							convMap[res.getID()] = id
							ressourceList.append(obj)
							
					if not obj in convMap.keys():
						id = resLst.createRecipeFromData(name = recipe.name, inputColl = [], outputColl = [], cost = recipe.getRecipeCost())
						convMap[recipe.getID()] = id
						coll_tmp += recipe.getParents()
						
			coll = set(list(coll_tmp))
		
		for recipe in recipeList:
			rec = self.getRecipe(recipe)
			
			for cnt, id in [(count, convMap[id]) for count, id in rec.input]:
				resLst.getRecipe(convMap[recipe]).addInput(cnt, id)
			for cnt, id in [(count, convMap[id]) for count, id in rec.output]:
				resLst.getRecipe(convMap[recipe]).addOutput(cnt, id)
		
		return resLst
	
	def getSubRecipeGraph(self, recipes):
		
		resLst = ResourceList()
		
		convMap = {}
		
		recipeList = []
		ressourceList = []
		
		for obj in recipes:
			recipe = self.getRecipe(obj)
			
			recipeList.append(obj)
			for rid in recipe.getOutputSet():
				if not (rid in convMap.keys()):
					res = self.getResource(rid)
					id = resLst.addResource(res.getName(), res.getValue())
					convMap[res.getID()] = id
					ressourceList.append(obj)
			for rid in recipe.getInputSet():
				if not (rid in convMap.keys()):
					res = self.getResource(rid)
					id = resLst.addResource(res.getName(), res.getValue())
					convMap[res.getID()] = id
					ressourceList.append(obj)
					
			if not obj in convMap.keys():
				id = resLst.createRecipeFromData(name = recipe.name, inputColl = [], outputColl = [], cost = recipe.getRecipeCost())
				convMap[recipe.getID()] = id
					
		for recipe in recipeList:
			rec = self.getRecipe(recipe)
			
			for cnt, id in [(count, convMap[id]) for count, id in rec.input]:
				resLst.getRecipe(convMap[recipe]).addInput(cnt, id)
			for cnt, id in [(count, convMap[id]) for count, id in rec.output]:
				resLst.getRecipe(convMap[recipe]).addOutput(cnt, id)
		
		return resLst