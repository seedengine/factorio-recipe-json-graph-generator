import argparse
import json
from datetime import datetime
import ResourceList
from lib.graph import GraphIO, GraphUtil

parser = argparse.ArgumentParser(description='Compute Recipe Paths from Factorio')
parser.add_argument('--path', dest="path", type=str, required=True,
                    help='Path to the computation config dir')
parser.add_argument('--recipes', dest="recipes", type=str, required=True,
                    help='path to the recipe json')

args = parser.parse_args()

with open( args.recipes, "r") as f:
	datasheet = json.load(f)
with open( args.path + "/config.json", "r") as f:
	config = json.load(f)

TARGET_RESOURCE = config["target"]


substanceTierList = []

substanceSet = [obj["name"] for obj in config["ingredients"]]

substanceTierList.append(substanceSet);

last = datetime.now().timestamp() * 1000

with open( "recipes.json", "r") as f:
	datasheet = json.load(f)
#if not k.startswith("y") and not ("barrel" in k) and not ("void" in k)
remainingSet = {k:v for (k, v) in datasheet.items() if 
	v["category"] != "fluid-filter" and 
	v["category"] != "barreling-pump" and 
	v["name"].find("barrel") == -1 and 
	not "ye_" in v["name"] and 
	not "yie_" in v["name"] and 
	not "ye-" in v["name"] and 
	not "y-" in v["name"] and 
	not "y_" in v["name"] and
	not "yi_" in v["name"] and
	not "yi-" in v["name"] and
	not "_ye" in v["name"] and 
	not "_yie" in v["name"] and 
	not "-ye" in v["name"] and 
	not "-y" in v["name"] and 
	not "_y" in v["name"] and
	not "_yi" in v["name"] and
	not "-yi" in v["name"] and
	not "converter" in v["name"] and
	not "from-y-" in v["name"] and
	not "from-ye_" in v["name"] and
	not "from-yie_" in v["name"] and
	not "from-ye-" in v["name"] and
	not "from-y_" in v["name"] and
	not "from-yi_" in v["name"] and
	v["category"] != "angels-water-void"}

now = datetime.now().timestamp() * 1000
print("Eliminated Yuki and Void recipes in " + str(now - last) + " milliseconds")
last = now

print("Building Graph with " + str(len(substanceTierList[0])) + " Root Items.")
print("Going to evaluate " + str(len(remainingSet)) + " recipes.")

graph = ResourceList.ResourceList()
for obj in config["ingredients"]:
	graph.addResource(obj["name"], obj["value"])


while True:
	newRecipes = {k:v for (k, v) in remainingSet.items() if set([c["name"] for c in v["ingredients"]]) <= set(substanceSet)}
	if (len(newRecipes.keys()) == 0):
		break
	for key in newRecipes.keys():
		del remainingSet[key]
		
	for (entry, value) in newRecipes.items():
		graph.createRecipeFromDictionary(value)
		
	newProducts = []
	for (k, v) in newRecipes.items():
		newProducts = newProducts + v["products"]
	newSubstances = [v["name"] for v in newProducts]
	substanceTierList.append([v for (k, v) in newRecipes.items()])
	substanceSet = list(set(substanceSet + newSubstances))

i = 0
for tier in substanceTierList:
	i += len(tier)
print("Sorted " + str(i) + " Recipes into " + str(len(substanceTierList)) + " Tiers.")

print(str(len(remainingSet.items())) + " Recipes got left over or couldn't be associated with the root Items.")
now = datetime.now().timestamp() * 1000
print("This took " + str(now - last) + " milliseconds")
last = now

if (len(remainingSet.items()) > 0):
	with open(args.path + "/remaining.json", "w+") as f:
		json.dump(remainingSet, f, indent=4)
	print("Dumped remaining recipes to remaining.json")

#GraphIO.writeGraphToFile(graph, "recipeGraph")
tmp = graph.getSubList(graph.getResourceFromName(TARGET_RESOURCE).getID())
baseResource = tmp.getResourceFromName(TARGET_RESOURCE)
loopCtr = 0
res = GraphUtil.findLoop(tmp, baseResource)
lastCnt = len(tmp.nodes)
while res:
	#print("Found Loop:")
	loopRecipeIDs = [id for id in res if tmp.isRecipe(id)]
	loopResourceIDs = [id for id in res if tmp.isResource(id)]
	
	#print([tmp.getRecipe(id).getName() for id in loopRecipeIDs])
	#print([tmp.getResource(id).getName() for id in loopResourceIDs])
	
	newRecipeGraph = tmp.getSubRecipeGraph(loopRecipeIDs)
	GraphIO.writeGraphToFile(newRecipeGraph, args.path + "/loopGraph." + str(loopCtr))
	
	#compute new recipe
	vec = {}
	val = 0
	for rid in loopRecipeIDs:
		r = tmp.getRecipe(rid)
		for cnt, in_id in r.input:
			if not in_id in vec.keys():
				vec[in_id] = -cnt
			else:
				vec[in_id] -= cnt
		
		for cnt, out_id in r.output:
			if not out_id in vec.keys():
				vec[out_id] = cnt
			else:
				vec[out_id] += cnt
		val += r.getRecipeCost()
	
	for id in loopRecipeIDs:
		tmp.deleteRecipe(id)
	
	#print({tmp.getResource(id).getName() : cnt for id, cnt in vec.items()})
	
	newInput = [(-value, id) for (id, value) in vec.items() if value > 0]
	newOutput = [(value, id) for (id, value) in vec.items() if value < 0]
	tmp.createRecipeFromData(name = "loop." + str(loopCtr), inputColl = newInput, outputColl = newOutput, cost = val)
	res = 0
	baseResource = tmp.getResourceFromName(TARGET_RESOURCE)
	lastCnt = len(tmp.nodes)
	#print([(obj.getID(), obj.getName()) for obj in tmp.nodes])
	res = GraphUtil.findLoop(tmp, baseResource)
	loopCtr += 1

print(len(tmp.nodes))
tmp.computeValue(tmp.getResourceFromName(TARGET_RESOURCE).getID())
print(tmp.getResourceFromName(TARGET_RESOURCE).value)
GraphIO.writeGraphToFile(tmp, args.path + "/recipeSubGraph")
